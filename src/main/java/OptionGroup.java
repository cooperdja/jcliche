package org.davec.jcliche;

import java.util.*;

/** 
 * <p>Represents a way of identifying a group of CLI options, for the purposes of displaying help
 * information.</p>
 *
 * @author David Cooper
 */
public class OptionGroup implements Comparable<OptionGroup>
{
    private final int order;
    private final String description;

    public OptionGroup(int order, String description) 
    {
        this.order = order;
        this.description = description;
    }
    
    public String getDescription() 
    { 
        return description; 
    }
    
    @Override
    public int compareTo(OptionGroup other)
    {
        int orderCmp = order - other.order;
        if(orderCmp == 0)
        {
            orderCmp = description.compareTo(other.description);
        }
        return orderCmp;
    }

    @Override
    public boolean equals(Object o)
    {
        if(!(o instanceof OptionGroup)) return false;
        
        OptionGroup og = (OptionGroup)o;
        return 
            order == og.order &&
            Objects.equals(description, og.description);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(description, order);
    }
    
    @Override
    public String toString()
    {
        return description;
    }
}
