package org.davec.jcliche;

/**
 * <p>An {@link ArgType} that parses an integer command-line argument, and validates it against 
 * optional upper and/or lower bounds.</p>
 *
 * <p>The following factory methods produce IntArgType instances:</p>
 * <ul>
 *   <li>{@link #noLimits()} returns an IntArgType that parses any integer but effectively performs
 *       no validation afterwards. That is, the value can be any representable integer value.</li>
 *
 *   <li>{@link #atLeast(int)} returns an IntArgType that checks its argument against a particular 
 *       minimum value.</li>
 *
 *   <li>{@link #between(int,int)} returns an IntArgType that checks its argument against specified
 *       minimum and maximum values.</li>
 * </ul>
 *
 * @author David Cooper
 */
public class IntArgType implements ArgType<Integer>
{
    private int min, max;
    
    public static IntArgType noLimits()
    {
        return new IntArgType(Integer.MIN_VALUE, Integer.MAX_VALUE);
    }
    
    public static IntArgType atLeast(int min)
    {
        return new IntArgType(min, Integer.MAX_VALUE);
    }
    
    public static IntArgType between(int min, int max)
    {
        return new IntArgType(min, max);
    }
    
    private IntArgType(int min, int max)
    {
        this.min = min;
        this.max = max;
    }

    @Override
    public Integer parse(String argStr) throws CLParseException
    {
        try
        {
            int arg = Integer.parseInt(argStr);
            if(arg < min)
            {
                throw new CLParseException(
                    String.format("Argument (\"%s\") is less than the minimum %d.", argStr, min));
            }
            if(arg > max)
            {
                throw new CLParseException(
                    String.format("Argument (\"%s\") is greater than the maximum %d.", argStr, max));
            }
            return arg;
        }
        catch(NumberFormatException e)
        {
            throw new CLParseException("Non-integer argument \"" + argStr + "\".", e);
        }
    }
}
