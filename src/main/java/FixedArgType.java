package org.davec.jcliche;

import java.util.*;

/**
 * An {@link ArgType} that (1) matches a supplied command-line argument against a fixed, 
 * pre-defined set of strings, and (2) returns a corresponding value.
 * 
 * @param <T> The datatype that the pre-defined strings map to.
 *
 * @author David Cooper
 */
public class FixedArgType<T> implements ArgType<T>
{
    private final Map<String,T> argMap = new HashMap<>();
    private boolean caseSensitive = true;

    public FixedArgType() {}
    
    public void setCaseSensitive(boolean caseSensitive)
    {
        if(argMap.size() > 0)
        {
            throw new IllegalStateException("Cannot set case sensitivity once argument mappings have been added.");
        }
        this.caseSensitive = caseSensitive;
    }

    public void addValue(String argStr, T argValue)
    {
        if(!caseSensitive)
        {
            argStr = argStr.toLowerCase();
        }
        argMap.put(argStr, argValue);
    }
    
    @Override
    public T parse(String argStr) throws CLParseException
    {
        if(!caseSensitive)
        {
            argStr = argStr.toLowerCase();
        }
        if(!argMap.containsKey(argStr))
        {
            throw new CLParseException("Unsupported value \"" + argStr + "\"");
        }
        return argMap.get(argStr);
    }        
}
