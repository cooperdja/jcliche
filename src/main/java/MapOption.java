package org.davec.jcliche;

import java.util.*;

/**
 * <p>A type of {@link ContainerOption}, representing a command-line option that takes an argument 
 * broken up into a set of key-value mappings, separated by a delimiter string.</p>
 *
 * <p>In order to parse each entry in the map, MapOption delegates to <em>another</em> 
 * {@link ArgType}. In practice, this is either {@link PairArgType} or {@link UnitArgType}, both
 * of which generate a {@link java.util.Map.Entry} (that being the constraint required by 
 * MapOption). Of these subclasses, PairArgType is more general, but requires the key and value to 
 * be separated by another kind of delimiter, while UnitArgType is designed specifically for the 
 * case when the values are real numbers and the keys have a unique string representation, and 
 * requires no delimiter.</p>
 *
 * @param <K> The key type (not to be confused with the ArgType that parses both the key and value 
 *            together).
 * @param <V> The value type.
 *
 * @author David Cooper
 */
public class MapOption<K,V> extends ContainerOption<Map.Entry<K,V>,Map<K,V>>
{
    private final ArgType<Map.Entry<K,V>> argType;
    private final Map<K,V> argMap = new HashMap<>();
    
    public MapOption(Character shortName, String longName, 
                     String argLabel, ArgType<Map.Entry<K,V>> argType, 
                     String description)
    {
        super(shortName, longName, argLabel, description);
        this.argType = argType;
    }

    public V getValue(K key, V defaultValue)
    {
        return argMap.containsKey(key) ? argMap.get(key) : defaultValue;
    }
    
    public Map<K,V> getMap()
    {
        return Collections.unmodifiableMap(argMap);
    }
    
    @Override
    protected ArgType<Map.Entry<K,V>> getArgType(int i)
    {
        return argType;
    }
    
    @Override
    protected void putElements(List<Map.Entry<K,V>> elements)
    {
        for(Map.Entry<K,V> el : elements)
        {
            argMap.put(el.getKey(), el.getValue());
        }
    }
    
    @Override
    protected Iterator<Map.Entry<K,V>> getIterator()
    {
        return argMap.entrySet().iterator();
    }
    
    @Override
    public void set(Map<K,V> values)
    {
        argMap.putAll(values);
    }
}
