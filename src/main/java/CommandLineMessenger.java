package org.davec.jcliche;

/**
 * <p>An interface designed to generate a message, for the purposes of displaying help, version, 
 * etc. information at the command line.</p>
 *
 * @author David Cooper
 */
public interface CommandLineMessenger
{
    boolean wordWrap();
    String getMessage(CountOption msgOp) throws CLParseException;
}
