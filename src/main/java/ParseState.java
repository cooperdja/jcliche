package org.davec.jcliche;

/**
 * <p>Represents the current parsing location within the command-line arguments. This class is 
 * only for internal use within org.davec.jcliche.</p>
 *
 * @author David Cooper
 */
class ParseState
{
    private String[] args;
    private int index;
    private String current;
    private String next = null;
    
    public ParseState(String[] args)
    {
        this.args = args;
        this.index = -1;
        this.current = "";
    }
    
    public int getArgIndex()
    {
        return index;
    }
    
    public boolean hasMoreArgs()
    {
        return next != null || index < (args.length - 1);
    }
    
    public void nextArg() throws ArrayIndexOutOfBoundsException
    {
        if(next == null)
        {
            index++;
            current = args[index];
        }
        else
        {
            current = next;
            next = null;
        }
    }
    
    public void pushArg()
    {
        pushArg(current);
    }
    
    public void pushArg(String chars)
    {
        if(next != null)
        {
            throw new IllegalStateException();
        }
        next = chars;
    }
    
    public boolean hasMoreChars()
    {
        return current.length() > 0;
    }
    
    public int charsLeft()
    {
        return current.length();
    }
    
    public char peekChar()
    {
        return current.charAt(0);
    }
    
    public char popChar()
    {
        char ch = current.charAt(0);
        current = current.substring(1);
        return ch;
    }
    
    public void discardChar()
    {
        current = current.substring(1);
    }
    
    public String peekChars()
    {
        return current;
    }
    
    public String popChars()
    {
        String chars = current;
        current = "";
        return chars;
    }
    
    public void discardChars()
    {
        current = "";
    }
    
    public void discardChars(int n)
    {
        current = current.substring(n);
    }
    
    public String popCharsUntil(String delimiter)
    {
        String chars;
        int delimiterPos = current.indexOf(delimiter);
        if(delimiterPos == -1)
        {
            chars = current;
            current = "";
        }
        else
        {
            chars = current.substring(0, delimiterPos);
            current = current.substring(delimiterPos);
        }        
        return chars;
    }    
}
