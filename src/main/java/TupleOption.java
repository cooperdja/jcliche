package org.davec.jcliche;

import java.util.*;

/**
 * <p>A type of {@link ContainerOption}, representing a command-line option that takes an argument 
 * broken up into fixed sequence of values. TupleOption is similar to {@link ListOption}, but is 
 * designed for when the number of values is pre-determined, and each value may have different 
 * constraints and its own specific default value. (Nonetheless, all values must still have a
 * common data type).<p>
 * 
 * @param <E> The type of each value in the tuple.
 *
 * @author David Cooper
 */
public class TupleOption<E> extends ContainerOption<E,List<E>>
{
    private final List<ArgType<E>> argTypes = new ArrayList<>();
    private final List<E> values = new ArrayList<>();
    private int nRequired = 0;
    private boolean nextRequired = true;
    
    public TupleOption(Character shortName, String longName, 
                       String argDescription, String description)
    {
        super(shortName, longName, argDescription, description);
        ellipsis(false);
    }

    public TupleOption<E> arg(ArgType<E> argType, E defaultValue)
    {
        argTypes.add(argType);
        values.add(defaultValue);
        if(nextRequired)
        {
            nRequired++;
        }
        maxItems(argTypes.size());
        return this;
    }
    
    public TupleOption<E> optionalArgs()
    {
        nextRequired = false;
        minItems(nRequired);
        return this;
    }
    
    public List<E> getList()
    {
        return Collections.unmodifiableList(values);
    }
    
    public E getValue(int i)
    {
        return values.get(i);
    }
    
    @Override
    protected ArgType<E> getArgType(int i) throws CLParseException
    {
        if(i >= argTypes.size())
        {
            throw new CLParseException(String.format(
                "Option \"%s\" cannot have more than %d values.",
                toString(), argTypes.size()));
        }
        return argTypes.get(i);
    }
    
    @Override
    protected void putElements(List<E> elements)
    {
        int index = 0;
        for(E el : elements)
        {
            values.set(index, el);
            index++;
        }
    }
    
    @Override
    protected Iterator<E> getIterator()
    {
        return values.iterator();
    }
    
    @Override
    public void set(List<E> values)
    {
        values.addAll(values);
    }
}
