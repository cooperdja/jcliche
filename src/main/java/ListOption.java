package org.davec.jcliche;

import java.util.*;

/**
 * <p>A type of {@link ContainerOption}, representing a command-line option that takes an argument 
 * broken up into sequence of values. ListOption is similar to {@link TupleOption}, but is 
 * designed for when the number of values is open-ended, and all values share the same set of 
 * constraints.<p>
 * 
 * @param <E> The type of each value in the list.
 *
 * @author David Cooper
 */
public class ListOption<E> extends ContainerOption<E,List<E>>
{
    private final ArgType<E> argType;
    private final List<E> values = new LinkedList<>();
    
    public ListOption(Character shortName, String longName, 
                      String argLabel, ArgType<E> argType, 
                      String description)
    {
        super(shortName, longName, argLabel, description);
        this.argType = argType;
    }
    
    public List<E> getList()
    {
        return Collections.unmodifiableList(values);
    }
    
    public int getNItems()
    {
        return values.size();
    }
    
    public E getItem(int i)
    {
        return values.get(i);
    }
    
    @Override
    protected ArgType<E> getArgType(int i)
    {
        return argType;
    }
    
    @Override
    protected void putElements(List<E> elements)
    {
        values.addAll(elements);
    }
    
    @Override
    protected Iterator<E> getIterator()
    {
        return values.iterator();
    }
    
    @Override
    public void set(List<E> values)
    {
        values.addAll(values);
    }
}
