package org.davec.jcliche;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>Represents some number (zero or more) of consecutive positional arguments on the 
 * command-line.</p>
 * 
 * @param <T> The datatype of the argument(s).
 * @author David Cooper
 */
public class Positional<T>
{
    private final String argName;
    private final ArgType<T> argType;
    private final int minArgs;
    private final Integer maxArgs;
    private final List<T> values = new LinkedList<>();
    
    /**
     * Creates a new Positional representing exactly one positional command-line arguments.
     *
     * @param <T> The data type (conceptually) of the command-line arguments.
     * @param argName The name of the positional, for help display purposes.
     * @param argType The particular {@link ArgType} that will parse the arguments to obtain a 
     *                value of type T.
     */
    public static <T> Positional<T> one(String argName, ArgType<T> argType)
    {
        return new Positional<>(1, 1, argName, argType);
    }
    
    /**
     * Creates a new Positional representing a fixed number of consecutive positional command-line 
     * arguments.
     *
     * @param <T> The data type (conceptually) of the command-line parameters.
     * @param nArgs The number of command-line arguments expected.
     * @param argName The name of the positional, for help display purposes.
     * @param argType The particular {@link ArgType} that will parse the parameters to obtain 
     *                values of type T.
     */
    public static <T> Positional<T> exactly(int nArgs, String argName, ArgType<T> argType)
    {
        return new Positional<>(nArgs, nArgs, argName, argType);
    }
    
    /**
     * Creates a new Positional representing a variable number of consecutive positional 
     * command-line arguments, between a particular minimum and maximum.
     *
     * @param <T> The data type (conceptually) of the command-line arguments.
     * @param minArgs The minimum number of command-line arguments expected.
     * @param maxArgs The maximum number of command-line arguments expected.
     * @param argName The name of the positional, for help display purposes.
     * @param argType The particular {@link ArgType} that will parse the parameters to obtain 
     *                values of type T.
     */
    public static <T> Positional<T> between(int minArgs, int maxArgs, String argName, 
                                                                      ArgType<T> argType)
    {
        return new Positional<>(minArgs, maxArgs, argName, argType);
    }
    
    /**
     * Creates a new Positional representing an unlimited number of consecutive positional 
     * command-line arguments, with at least a particular minimum number.
     *
     * @param <T> The data type (conceptually) of the command-line arguments.
     * @param minArgs The minimum number of command-line arguments expected.
     * @param argName The name of the positional, for help display purposes.
     * @param argType The particular {@link ArgType} that will parse the parameters to obtain 
     *                values of type T.
     */
    public static <T> Positional<T> atLeast(int minArgs, String argName, ArgType<T> argType)
    {
        return new Positional<>(minArgs, null, argName, argType);
    }
    
    /**
     * Creates a new Positional representing a number of consecutive positional command-line 
     * arguments. 
     *
     * @param minArgs The minimum number of command-line arguments expected.
     * @param maxArgs The maximum number of command-line arguments expected, or null if there is
     *                no particular maximum.
     * @param argName The name of the positional, for help display purposes.
     * @param argType The particular {@link ArgType} that will parse the parameters to obtain 
     *                values of type T.
     *
     * @throws IllegalArgumentException If minArgs is negative, or is greater than maxArgs (and 
     *                                  maxArgs is not null).
     */
    public Positional(int minArgs, Integer maxArgs, String argName, ArgType<T> argType)
    {
        if(minArgs < 0)
        {
            throw new IllegalArgumentException(
                String.format("minArgs (%d) should not be negative", minArgs));
        }
        
        if(maxArgs != null && minArgs > maxArgs)
        {
            throw new IllegalArgumentException(
                String.format("minArgs (%d) should not be higher than maxArgs (%d)", 
                              minArgs, maxArgs));
        }

        this.argName = argName;
        this.argType = argType;
        this.minArgs = minArgs;
        this.maxArgs = maxArgs;
    }
    
    public int getMinArgs()     { return minArgs; }
    public Integer getMaxArgs() { return maxArgs; }
    public String getArgName()  { return argName; }
    
    public boolean parseNext(String arg) throws CLParseException
    {
        boolean added = false;
        int nValues = values.size();
        if(maxArgs == null || nValues < maxArgs)
        {
            try
            {
                values.add(argType.parse(arg));
                added = true;
            }
            catch(CLParseException e)
            {
                if(nValues < minArgs)
                {
                    throw e;
                }
                // Otherwise, if we already have enough arguments, and the next one is unparsable, 
                // then we'll return false and assume it's someone else's problem (i.e. the next
                // Positional, or if there is none, then CommandLineParser).
            }
        }
        return added;
    }
    
    /**
     * <p>Called after the final argument has been provided to this positional. This does not 
     * necessarily mean that the command-line parser has completely run out of arguments. Rather, 
     * either (a) it cannot afford to supply any more arguments to this positional, because there
     * wouldn't be enough left to be supplied to others, OR (b) the previous call to parseNext() 
     * returned false, meaning that this positional does not want any more arguments.</p>
     * 
     * <p>The method either simply does nothing, or throws an exception.</p>
     * 
     * @throws CLParseException If this positional did not get its minimum number of 
     *                               arguments.
     */
    public void lastArg() throws CLParseException
    {
        int nValues = values.size();
        if(nValues < minArgs)
        {
            throw new CLParseException(
                String.format("Expected at least %d %s argument(s) but only found %d", 
                              minArgs, argName, nValues));
        }
    }
    
    public void set(Collection<T> values)
    {
        this.values.clear();
        this.values.addAll(values);
    }
    
    public List<T> get()
    {
        return Collections.unmodifiableList(values);
    }
    
    public T getFirst()
    {
        return values.get(0);
    }
    
    public String formatValue()
    {
        StringBuilder sb = new StringBuilder();
        Iterator<T> it = values.iterator();
        if(it.hasNext())
        {
            sb.append(it.next());
        }
        while(it.hasNext())
        {
            sb.append(" ");
            sb.append(it.next());
        }
        return sb.toString();
    }
}
