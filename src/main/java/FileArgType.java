package org.davec.jcliche;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>An {@link ArgType} that parses and (to an extent) validates a filename or directory-name 
 * command-line argument.</p>
 *
 * <p>Several pre-defined FileArgTypes are available via {@link #exists(boolean)}, 
 * {@link #isFile(boolean)}, {@link #isDirectory(boolean)}, {@link #readable(boolean)}, 
 * {@link #writable(boolean)} and {@link #creatable(boolean)}. These each perform a simple check to
 * on the argument for validation purposes. In each case, if the parameter is {@code false}, the 
 * meaning (and the check) is inverted. These basic types can be aggregated hierarchically with
 * {@link #all(FileArgType...)} and {@link #any(FileArgType...)}.<p>
 *
 * <p>If validation fails, the various instances of FileArgType throw an 
 * {@link CLParseException} with a reasonably precise error message.</p>
 *
 * @author David Cooper
 */
public abstract class FileArgType implements ArgType<File>
{
    public static FileArgType exists(final boolean shouldExist)
    {
        return new FileArgType()
        {
            @Override
            public void validate(File f) throws CLParseException
            {
                if(f.exists() != shouldExist)
                    throw new CLParseException(
                        f.exists() ? "exists" : "does not exist");
            }
        };
    }
    
    public static FileArgType isFile(final boolean shouldBeFile)
    {
        return new FileArgType()
        {
            @Override
            public void validate(File f) throws CLParseException
            {
                if(f.isFile() != shouldBeFile)
                    throw new CLParseException(
                        f.isFile() ? "is an ordinary file" : "is not an ordinary file");
            }
        };
    }

    public static FileArgType isDirectory(final boolean shouldBeDir)
    {
        return new FileArgType()
        {
            @Override
            public void validate(File f) throws CLParseException
            {
                if(f.isDirectory() != shouldBeDir)
                    throw new CLParseException(
                        f.isDirectory() ? "is a directory" : "is not a directory");
            }
        };
    }
    
    public static FileArgType readable(final boolean shouldBeReadable)
    {
        return new FileArgType()
        {
            @Override
            public void validate(File f) throws CLParseException
            {
                if(f.canRead() != shouldBeReadable)
                    throw new CLParseException(
                        f.canRead() ? "is readable" : "is not readable");
            }
        };
    }
    
    public static FileArgType writable(final boolean shouldBeWritable)
    {
        return new FileArgType()
        {
            @Override
            public void validate(File f) throws CLParseException
            {
                if(f.canWrite() != shouldBeWritable)
                    throw new CLParseException(
                        f.canWrite() ? "is writable" : "is not writable");
            }
        };
    }
    
    public static FileArgType creatable(final boolean shouldBeCreatable)
    {
        return new FileArgType()
        {
            @Override
            public void validate(File f) throws CLParseException
            {
                if(shouldBeCreatable)
                {
                    if(f.exists())
                        throw new CLParseException("already exists");
                        
                    if(!f.getParentFile().canWrite())
                        throw new CLParseException("not in a writable directory");
                }
                else
                {
                    if(!f.exists() || f.getParentFile().canWrite())
                    {
                        throw new CLParseException(
                            "does not exist, and directory is writable");
                    }
                }
            }
        };
    }
    
    public static FileArgType exists()      { return exists(true); }
    public static FileArgType isFile()      { return isFile(true); }
    public static FileArgType isDirectory() { return isDirectory(true); }
    public static FileArgType readable()    { return readable(true); }
    public static FileArgType writable()    { return writable(true); }
    public static FileArgType creatable()   { return creatable(true); }
    
    
    public static FileArgType all(final FileArgType... args)
    {
        return new FileArgType()
        {
            @Override
            public void validate(File f) throws CLParseException
            {
                for(FileArgType arg : args)
                {
                    arg.validate(f);
                }
            }
        };
    }
    
    public static FileArgType any(final FileArgType... args)
    {
        return new FileArgType()
        {
            @Override
            public void validate(File f) throws CLParseException
            {
                StringBuilder sb = new StringBuilder();
                boolean firstMsg = true;
                
                for(FileArgType arg : args)
                {
                    try
                    {
                        arg.validate(f);
                        return; // We only need one to match.
                    }
                    catch(CLParseException e)
                    {
                        if(!firstMsg)
                        {
                            sb.append(", ");
                        }
                        else
                        {
                            firstMsg = false;
                        }
                        sb.append(e.getMessage());
                    }
                }
                throw new CLParseException(sb.toString());
            }
        };
    }
    
    
    protected FileArgType() {}

    @Override
    public File parse(String argStr) throws CLParseException
    {
        File arg = new File(argStr).getAbsoluteFile();
        try
        {
            validate(arg);
            return arg;
        }
        catch(CLParseException e)
        {
            throw new CLParseException(String.format("\"%s\": %s", argStr, e.getMessage()), e);
        }
    }
    
    protected abstract void validate(File f) throws CLParseException;
}
