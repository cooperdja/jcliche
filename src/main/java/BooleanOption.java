package org.davec.jcliche;

import java.util.*;

/** 
 * <p>A type of {@link Option} that represents a yes/no value. It takes one optional argument, 
 * which (by default) can be "y" or "yes" for true, or "n" or "no" for false 
 * (case-insensitive). These values, and whether or not they are case-sensitive, are 
 * configurable.</p>
 *
 * <p>Two defaults can be configured: one for if the option is omitted altogether, and another for
 * if the option is present but the argument is missing.</p>
 *
 * @author David Cooper
 */
public class BooleanOption extends Option<Boolean>
{
    private boolean specified;
    private boolean defaultNoArg;
    private boolean caseSensitive;
    private Set<String> trueValues = new HashSet<>();
    private Set<String> falseValues = new HashSet<>();
    private String formatTrueValue;
    private String formatFalseValue;

    public BooleanOption(Character shortName, String longName, 
        boolean defaultNoOption, boolean defaultNoArg, String description)
    {
        super(shortName, longName, description);
        this.specified = defaultNoOption;
        this.defaultNoArg = defaultNoArg;
        caseSensitive = false;
        setTrueValues("y", "yes");
        setFalseValues("n", "no");
    }
    
    public void setCaseSensitive(boolean caseSensitive)
    {
        this.caseSensitive = caseSensitive;
    }
    
    private void setValues(Set<String> ourValues, String... newValues)
    {
        ourValues.clear();
        for(String v : newValues)
        {
            if(!caseSensitive)
            {
                v = v.toLowerCase();
            }
            ourValues.add(v);
        }
    }
    
    public void setTrueValues(String... newValues)
    {
        setValues(this.trueValues, newValues);
        formatTrueValue = newValues[0];
    }
    
    public void setFalseValues(String... newValues)
    {
        setValues(this.falseValues, newValues);
        formatFalseValue = newValues[0];
    }
        
    @Override
    public void parse(String opStr, ParseState state)
    {
        if(!state.hasMoreChars() && state.hasMoreArgs())
        {
            state.nextArg();
        }
        String arg = state.peekChars();
        if(!caseSensitive)
        {
            arg = arg.toLowerCase();
        }
        
        if(trueValues.contains(arg))
        {
            specified = true;
            state.discardChars();
        }
        else if(falseValues.contains(arg))
        {
            specified = false;
            state.discardChars();
        }
        else
        {
            specified = defaultNoArg;
        }
    }
    
    @Override
    public void parseWithArg(String opStr, String explicitArg, ParseState state) 
        throws CLParseException
    {
        specified = trueValues.contains(explicitArg);
        if(!specified && !falseValues.contains(explicitArg))
        {
            throw new CLParseException(String.format(
                "Option \"%s\": Unsupported value \"%s\".", 
                opStr, explicitArg));
        }
    }    
    
    @Override
    public String getArgDescription(String delimiter)
    {
        return String.format(
            "%s[%s%s/%s]", 
            (delimiter == null ? " " : ""),
            (delimiter == null ? "" : delimiter), 
            formatTrueValue, 
            formatFalseValue);
    }
            
    public boolean wasSpecified()
    {
        return specified;
    }
        
    @Override
    public void set(Boolean value) throws CLParseException
    {
        this.specified = value;
    }
    
    @Override
    public String formatValue()
    {
        return String.valueOf(specified);
    }
}
