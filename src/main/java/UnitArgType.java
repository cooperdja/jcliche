package org.davec.jcliche;

import java.util.*;
import java.util.regex.*;

/**
 * <p>A type of {@link ArgType} representing a command-line argument that has two components: a 
 * real number, and a unit. The unit must be one of a fixed set of strings.</p>
 * 
 * @param <K> A datatype representing the different kinds of units.
 *
 * @author David Cooper
 */
public class UnitArgType<K> implements ArgType<Map.Entry<K,Double>>
{
    private static final Pattern PATTERN = Pattern.compile("(?<number>\\.\\d+|\\d+(\\.\\d+)?)(?<unit>.*)");
    private final Map<String,K> unitMap = new HashMap<>();
    private final Map<String,Double> minMap = new HashMap<>();
    private final Map<String,Double> maxMap = new HashMap<>();

    public UnitArgType() {}
    
    public void addUnit(String unitStr, K unitRepr, double min, double max)
    {
        unitMap.put(unitStr, unitRepr);
        minMap.put(unitStr, min);
        maxMap.put(unitStr, max);
    }
    
    public void addUnit(String unitStr, K unitRepr, double min)
    {
        addUnit(unitStr, unitRepr, min, Double.POSITIVE_INFINITY);
    }
    
    public void addUnit(String unitStr, K unitRepr)
    {
        addUnit(unitStr, unitRepr, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
    }
    
    @Override
    public Map.Entry<K,Double> parse(String argStr) throws CLParseException
    {
        Matcher m = PATTERN.matcher(argStr);
        if(!m.matches())
        {
            throw new CLParseException("Invalid number-unit argument \"" + argStr + "\".");
        }
            
        double value = Double.parseDouble(m.group("number"));
        
        String unitStr = m.group("unit");
        if(!unitMap.containsKey(unitStr))
        {
            throw new CLParseException("Unsupported unit \"" + unitStr + "\"");
        }
        
        double min = minMap.get(unitStr);
        if(value < min)
        {
            throw new CLParseException(
                String.format("Argument (\"%s\") is less than the minimum %f%s.", argStr, min, unitStr));
        }
        
        double max = maxMap.get(unitStr);
        if(value > max)
        {
            throw new CLParseException(
                String.format("Argument (\"%s\") is greater than the maximum %f%s.", argStr, max, unitStr));
        }
            
        return new AbstractMap.SimpleImmutableEntry<>(unitMap.get(unitStr), value);
    }     
}
