package org.davec.jcliche;

import java.util.Objects;

/**
 * <p>Represents a command-line option (such as "-p" or "--option"), with some associated 
 * value.</p>
 * 
 * @param <T> The type of associated value. A value isn't mandatory; T could be {@link Void} if 
 * the argument is simply a flag.
 *
 * @author David Cooper
 */
public abstract class Option<T> implements Comparable<Option<T>>
{
    private Character shortName = null;
    private String longName = null;
    private String description = null;

    public Option(Character shortName, String longName, String description)
    {
        this.shortName = shortName;
        this.longName = longName;
        this.description = description;
    }
    
    public Character getShortName() { return shortName; }        
    public String getLongName()     { return longName; }
    public String getDescription()  { return description; }    

    public void setDescription(String newDescription)
    {
        description = newDescription;
    }
    
    public abstract void parse(String opStr, ParseState state) throws CLParseException;
    public abstract void parseWithArg(String opStr, String explicitArg, ParseState state) 
        throws CLParseException;
        
    public abstract void set(T value) throws CLParseException;    
    public abstract String getArgDescription(String delimiter);
    public abstract String formatValue();
    public void endCheck() throws CLParseException {}
    
    @Override
    public boolean equals(Object other)
    {
        if(!(other instanceof Option)) return false;
        
        Option<?> otherOp = (Option<?>)other;
        return Objects.equals(shortName, otherOp.shortName) &&
               Objects.equals(longName, otherOp.longName);
    }
    
    @Override
    public int compareTo(Option<T> other)
    {
        String first  = (longName       == null) ? "\0" : longName;
        String second = (other.longName == null) ? "\0" : other.longName;
        first =  ((shortName       == null) ? first.charAt(0)  : shortName)       + first;
        second = ((other.shortName == null) ? second.charAt(0) : other.shortName) + second;
        return first.compareTo(second);
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hash(shortName, longName);
    }
    
    @Override
    public String toString()
    {
        if(shortName != null && longName != null)
        {
            return String.format("-%c/--%s", shortName, longName);
        }
        else if(shortName != null)
        {
            return String.format("-%c", shortName);
        }
        else if(longName != null)
        {
            return String.format("--%s", longName);
        }
        else
        {
            return "unnamed option";
        }
    }
}
