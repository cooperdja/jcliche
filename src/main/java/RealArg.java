package org.davec.jcliche;

/**
 * <p>An {@link ArgType} that parses a real-number command-line argument, and validates it against 
 * optional upper and/or lower bounds.</p>
 *
 * <p>The following factory methods produce RealArgType instances:</p>
 * <ul>
 *   <li>{@link #noLimits()} returns an RealArgType that parses any real number but effectively 
 *       performs no validation afterwards. That is, the value can be any representable 
 *       {@code double} value.</li>
 *
 *   <li>{@link #atLeast(double)} returns an RealArgType that checks its argument against a 
 *       particular minimum value.</li>
 *
 *   <li>{@link #between(double,double)} returns an RealArgType that checks its argument against 
 *       specified minimum and maximum values.</li>
 * </ul>
 *
 * @author David Cooper
 */
public class RealArg implements ArgType<Double>
{
    private double min, max;
    
    public static RealArg noLimits()
    {
        return new RealArg(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
    }
    
    public static RealArg atLeast(double min)
    {
        return new RealArg(min, Double.POSITIVE_INFINITY);
    }
    
    public static RealArg between(double min, double max)
    {
        return new RealArg(min, max);
    }
    
    private RealArg(double min, double max)
    {
        this.min = min;
        this.max = max;
    }

    @Override
    public Double parse(String argStr) throws CLParseException
    {
        try
        {
            double arg = Double.parseDouble(argStr);
            if(arg < min)
            {
                throw new CLParseException(
                    String.format("Argument (\"%s\") is less than the minimum %f.", argStr, min));
            }
            if(arg > max)
            {
                throw new CLParseException(
                    String.format("Argument (\"%s\") is greater than the maximum %f.", argStr, max));
            }
            return arg;
        }
        catch(NumberFormatException e)
        {
            throw new CLParseException("Non-numeric argument \"" + argStr + "\".", e);
        }
    }
}
