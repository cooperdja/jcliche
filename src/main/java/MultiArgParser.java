package org.davec.jcliche;

import java.util.List;

/**
 * <p>Represents a parser that extracts a list of arguments, given the current ParseState. This is
 * currently only used by ContainerOption and its subclasses. There are at least a couple of 
 * strategies for doing this.</p>
 *
 * @author David Cooper
 */ 
public abstract class MultiArgParser
{
    private boolean optionDelimiter;
    private String argDelimiter;
    
    protected MultiArgParser(boolean optionDelimiter, String argDelimiter)
    {
        this.optionDelimiter = optionDelimiter;
        this.argDelimiter = argDelimiter;
    }
    
    public boolean isOptionDelimiterUsed()
    {
        return optionDelimiter;
    }
    
    public String getArgDelimiter()
    {
        return argDelimiter;
    }

    public abstract List<String> parse(ParseState state) throws CLParseException;
    public abstract List<String> parseWithArg(String explicitArg, ParseState state) 
        throws CLParseException;
}
