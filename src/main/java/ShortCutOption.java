package org.davec.jcliche;

/**
 * <p>Represents a command-line option intended to serve as a short-hand for a range of other 
 * options, or some other side-effect to be executed during parsing.</p>
 *
 * <p>This is achieved by subclassing ShortCutOption and overriding the {@link #set()} method.
 * The idea is to write a {@code set()} method, in the subclass, that calls the {@code set(...)} 
 * method(s) on the relevant other {@link Option} objects. Alternatively, the {@code set()} method
 * could perform some other actions that you want to take effect immediately (although it must do
 * so without knowing what the full parsed command-line is.)</p>
 * 
 * @author David Cooper
 */
public abstract class ShortCutOption extends CountOption
{
    public ShortCutOption(Character shortName, String longName, String description)
    {
       super(shortName, longName, description);
    }
    
    @Override
    public void parse(String optionStr, ParseState state) throws CLParseException
    {
        super.parse(optionStr, state);
        set();
    }
    
    @Override
    public void parseWithArg(String optionStr, String explicitArg, ParseState state) 
        throws CLParseException
    {
        super.parseWithArg(optionStr, explicitArg, state);
        set();
    }
    
    @Override
    public final void set(Void value) throws CLParseException
    {
        super.set(value);
        set();
    }
    
    public abstract void set() throws CLParseException;
}
